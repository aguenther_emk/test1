package de.emk.myema.ss.rest.json;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class JsonMapperTest
{
	@Test
	public void testParseNull()
	{
		String message = null;
		Map<String, String> resultMap = JsonMapper.parse(message);
		assertNotNull(resultMap);
	}
	@Test
	public void testParseNoObject()
	{
		String message = "";
		Map<String, String> resultMap = JsonMapper.parse(message);
		assertNotNull(resultMap);
		assertEquals(0, resultMap.size());
	}
	@Test
	public void testParseSimple()
	{
		String message = "{\"Name\":\"Andre\"}";
		Map<String, String> resultMap = JsonMapper.parse(message);
		assertNotNull(resultMap);
		assertEquals(1, resultMap.size());
		assertEquals("Andre", resultMap.get("Name"));
	}
	@Test
	public void testParseObject()
	{
		String message = "{\"Person\":{\"Name\":\"Andre\"}}";
		Map<String, String> resultMap = JsonMapper.parse(message);
		assertNotNull(resultMap);
		assertEquals(1, resultMap.size());
		assertEquals("Andre", resultMap.get("Person.Name"));
	}
	@Test
	public void testParseArray()
	{
		String message = "{\"Person\":[\"Andre\",\"Max\"]}";
		Map<String, String> resultMap = JsonMapper.parse(message);
		assertNotNull(resultMap);
		assertEquals(3, resultMap.size());
		assertEquals("2", resultMap.get("Person[].count"));
		assertEquals("Andre", resultMap.get("Person[0]"));
		assertEquals("Max", resultMap.get("Person[1]"));
	}
	@Test
	public void testParseComplex()
	{
		String message = "{\"Person\":{\"Alle Vornamen\" : [\"Andre\",\"Max\"], \"Nachname\":\"Meier\"}}";
		Map<String, String> resultMap = JsonMapper.parse(message);
		assertNotNull(resultMap);
		assertEquals(4, resultMap.size());
		assertEquals("2", resultMap.get("Person.Alle Vornamen[].count"));
		assertEquals("Andre", resultMap.get("Person.Alle Vornamen[0]"));
		assertEquals("Max", resultMap.get("Person.Alle Vornamen[1]"));
		assertEquals("Meier", resultMap.get("Person.Nachname"));
	}
	@Test
	public void testBuildNull()
	{
		String message = JsonMapper.build(null);
		assertNull(message);
	}
	@Test
	public void testBuildNoObject()
	{
		String message = JsonMapper.build(new HashMap<String, String>());
		assertNotNull(message);
		assertEquals("{}", message);
	}
	@Test
	public void testBuildSimple()
	{
		Map<String, String> req = new HashMap<String, String>();
		req.put("Name", "Andre");
		String message = JsonMapper.build(req);
		assertNotNull(message);
		assertEquals("{\"Name\":\"Andre\"}", message);
	}
	@Test
	public void testBuildObject()
	{
		Map<String, String> req = new HashMap<String, String>();
		req.put("Person.Name", "Andre");
		String message = JsonMapper.build(req);
		assertNotNull(message);
		assertEquals("{\"Person\":{\"Name\":\"Andre\"}}", message);
	}
	@Test
	public void testBuildArray()
	{
		Map<String, String> req = new HashMap<String, String>();
		req.put("Person.Name[0]", "Andre");
		req.put("Person.Name[1]", "Max");
		String message = JsonMapper.build(req);
		assertNotNull(message);
		assertEquals("{\"Person\":{\"Name\":[\"Andre\",\"Max\"]}}", message);
	}
	@Test
	public void testBuildArrayMoreComplex()
	{
		Map<String, String> req = new HashMap<String, String>();
		req.put("Person.Name[0].X", "Andre");
		req.put("Person.Name[1].Y", "Max");
		String message = JsonMapper.build(req);
		assertNotNull(message);
		assertEquals("{\"Person\":{\"Name\":[{\"X\":\"Andre\"},{\"Y\":\"Max\"}]}}", message);
	}
	@Test
	public void testBuildArrayMuchMoreComplex()
	{
		Map<String, String> req = new HashMap<String, String>();
		req.put("Person.Name[0].X.X", "Andre");
		req.put("Person.Name[1].Y.Y", "Max");
		String message = JsonMapper.build(req);
		assertNotNull(message);
		assertEquals("{\"Person\":{\"Name\":[{\"X\":{\"X\":\"Andre\"}},{\"Y\":{\"Y\":\"Max\"}}]}}", message);
	}
	@Test
	public void testBuildArrayMuchMoreComplex2()
	{
		Map<String, String> req = new HashMap<String, String>();
		req.put("Person.Adresse[0].Strasse", "XStrasse");
		req.put("Person.Adresse[0].Ort", "XOrt");
		req.put("Person.Adresse[0].Hausnummer", "XHausnummer");
		String message = JsonMapper.build(req);
		assertNotNull(message);
		assertEquals("{\"Person\":{\"Adresse\":[{\"Hausnummer\":\"XHausnummer\",\"Ort\":\"XOrt\",\"Strasse\":\"XStrasse\"}]}}", message);
	}

}
