package de.emk.myema.ss.rest.serverhandler;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyemaClientPublicTest
{
	Logger log = LoggerFactory.getLogger(this.getClass().getName());
	SimpleHttpClient httpClient = null;
	
	@Before
	public void setUp()
	{
		httpClient = new SimpleHttpClient();
	}
	
	@Test
	public void test_GOOGLE_SSL() throws Exception
	{
		MyemaClient client = new MyemaClient(() -> {return httpClient;},"https://www.google.de/?gws_rd=ssl", null, null);
		SimpleHttpResponse content = client.request("GET", null, null);
		assertNotNull(content);
	}

	@Test
	public void test_HEISE_NOSSL() throws Exception
	{
		MyemaClient client = new MyemaClient(() -> {return httpClient;},"http://www.heise.de/", null, null);
		SimpleHttpResponse content = client.request("GET", null, null);
		assertNotNull(content);
	}

}
