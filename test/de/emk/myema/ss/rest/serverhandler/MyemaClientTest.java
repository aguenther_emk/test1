package de.emk.myema.ss.rest.serverhandler;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyemaClientTest
{
	Logger log = LoggerFactory.getLogger(this.getClass().getName());
	SimpleHttpClient httpClient = null;
	
	@Before
	public void setUp()
	{
		httpClient = new SimpleHttpClient();
	}

	@Test
	public void testLOGIN() throws Exception
	{
		MyemaClient client = new MyemaClient(() -> {return httpClient;},"http://127.0.0.1:8081/rest", null, null);
		SimpleHttpResponse response = client.request("POST", "/$directory/login", "[\"_internal\",  \"_internal2014\"]");
		assertNotNull(response);
		assertEquals(new Integer(200), response.getStatus());
		assertNotNull(response.getContent());
	}

	@Test
	public void testSequence() throws Exception
	{
		MyemaClient client = new MyemaClient(() -> {return httpClient;},"http://127.0.0.1:8081/rest", "_internal", "_internal2014");
		assertTrue(client.login());
		assertTrue(client.isConnected());
		SimpleHttpResponse response  = client.request("GET", "/$catalog", null);
		System.out.println(response.getContent());
	}
}
