package de.emk.myema.ss.rest;

import static spark.Spark.*;
import de.emk.myema.ss.rest.requesthandler.*;

public class RestServer
{
	public static void main(String[] args)
	{
        get("/Person", (req, res) -> new RequestHandlerAllPerson().handleRequest(req, res).body());
        get("/Person/:id", (req, res) -> new RequestHandlerOnePerson().handleRequest(req, res).body());
	}
}
