package de.emk.myema.ss.rest.requesthandler;

import spark.Request;
import spark.Response;

public interface RequestHandler
{
	public Response handleRequest(Request req, Response resp);
}
