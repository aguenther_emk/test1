package de.emk.myema.ss.rest.requesthandler;

import java.util.Map;

import de.emk.myema.ss.rest.json.JsonMapper;
import de.emk.myema.ss.rest.serverhandler.MyemaClient;
import de.emk.myema.ss.rest.serverhandler.SimpleHttpResponse;
import spark.Request;
import spark.Response;

public class RequestHandlerOnePerson extends RequestHandlerBase implements RequestHandler
{

	@Override
	public Response handleRequest(Request request, Response response)
	{
		// id ermitteln
		String id = request.params(":id");
		
		// Headerparams ermitteln 
		String benutzer = request.headers("Benutzer"); 
		String password = request.headers("Passwort");
		String api_key  = request.headers("AKI-KEY");
		Integer limit   = getIntValue(request.headers("limit"));
		Integer startid = getIntValue(request.headers("startId"));
		
		// Grunddaten Daten aus Wakanda holen
//		MyemaClient client = MyemaClientStore.get(benutzer, password);
//		SimpleHttpResponse wakandaGeneralResp = client.request("GET", urlExt, payload);
//		Map<String, String> parsedMessage = JsonMapper.parse(wakandaGeneralResp.getContent());
		
		// anreichern der Daten
		
		
		// Json bauen 
		
		
		response.body("One Persons");
		return response;
	}

	private Integer getIntValue(String value)
	{
		try
		{
			return Integer.parseInt(value);
		}
		catch (Exception e)
		{
			
		}
		return null;
	}

}
