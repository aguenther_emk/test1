package de.emk.myema.ss.rest.requesthandler;

import spark.Request;
import spark.Response;

public class RequestHandlerAllPerson extends RequestHandlerBase implements RequestHandler
{
	@Override
	public Response handleRequest(Request req, Response resp)
	{
		resp.body("All Persons");
		return resp;
	}

}
