package de.emk.myema.ss.rest.json;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

public class JsonMapper
{
	public static Map<String, String> parse (String message)
	{
		Map<String, String> props = new HashMap<String, String> ();
		
		if (message == null)
			return props;
		
		try
		{
			JsonFactory f = new JsonFactory();
			JsonParser jp = f.createParser(message);
			jp.nextToken();
			JsonMapper.parse(null,jp, props, null);
			jp.close(); 
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return props; 
	}
	
	private static void parse(String path,
			JsonParser jp, 
			Map<String, String>  erg, 
			String arrayCounter) throws JsonParseException, IOException
	{
		JsonToken aktToken = jp.nextToken(); 
		while (aktToken != null)
		{
			String aktPath = (path == null ? "" : path + ".") + jp.getCurrentName();
			if (arrayCounter  != null && aktToken != JsonToken.END_ARRAY)
			{
				String aktCounter = erg.get(arrayCounter);
				if (aktCounter == null)               
					aktCounter = "0";
				else
					aktCounter = "" + (Integer.parseInt(aktCounter)+1);
				erg.put(arrayCounter, aktCounter);
				aktPath = (path == null ? "" : path ) + "["+ aktCounter+ "]";               
			}
			// am EndedesArrays muessen wir den Count noch um eins erhoehen - wir haben ja mit Index = 0 angefangen
			if (arrayCounter  != null && aktToken == JsonToken.END_ARRAY)
			{
				String aktCounter = erg.get(arrayCounter);
				if (aktCounter == null)               
					aktCounter = "0";
				else
					aktCounter = "" + (Integer.parseInt(aktCounter)+1);
				erg.put(arrayCounter, aktCounter);
			}
			switch(aktToken)
			{
				case FIELD_NAME:
					break;
				case START_OBJECT: 
					JsonMapper.parse(aktPath,jp, erg, null);
					break;
				case END_OBJECT: 
				case END_ARRAY:
					return;
				case START_ARRAY:
					JsonMapper.parse(aktPath,jp, erg, aktPath + "[].count");
					break;
				case VALUE_STRING:
				default:
					erg.put(aktPath , jp.getText());        
					break;
			}
			aktToken = jp.nextToken();
		}
	}
	
	
	public static String build(Map<String, String> props) 
	{
		if (props == null)
			return null;
		
		StringWriter stw = new StringWriter();

		try
		{
			JsonFactory f = new JsonFactory();
			JsonGenerator g = f.createGenerator(stw);
			ArrayList<String> keys = new ArrayList<String>(props.keySet());
			keys.sort(String::compareTo);

			g.writeStartObject();
	
			Iterator<String> it = keys.iterator();
			while (it.hasNext())            
			{
				String[] aktKey = getNextKey (it);
				aktKey = handleEbene(aktKey, 0, props, it, g,false);
			}
			g.close(); 
	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	
		System.out.println(stw.toString());
		return stw.toString();
	}


	private static  String[] handleEbene(String[] aktKeyArray,
			int ebene,
			Map<String, String> props, 
			Iterator<String> keyIterator,
			JsonGenerator g,
			boolean parrentIsArrayElement ) throws JsonGenerationException, NumberFormatException, IOException
	{
		String name = aktKeyArray[ebene];
		boolean arrayIsOpen = false;
		boolean arrayIsSameIndex = false;
		boolean objectIsOpen = false;
		
		while (ebene < aktKeyArray.length && ohneKlammern(aktKeyArray[ebene]).compareTo(ohneKlammern(name)) == 0)
		{
			if (isArray(aktKeyArray[ebene]) && arrayIsOpen)
			{
				if (ebene == aktKeyArray.length -1)
					g.writeString(props.get(Arrays.stream(aktKeyArray).collect(Collectors.joining("."))));
				else
				if (!arrayIsSameIndex)
				{
					g.writeStartObject();
					objectIsOpen = true;
				}
			}
			else
			if (isArray(aktKeyArray[ebene]) && !arrayIsOpen)
			{
				g.writeArrayFieldStart(ohneKlammern(aktKeyArray[ebene]));
				arrayIsOpen = true;
				if (ebene == aktKeyArray.length -1)
					g.writeString(props.get(Arrays.stream(aktKeyArray).collect(Collectors.joining("."))));
				else
				if (!arrayIsSameIndex)
				{
					g.writeStartObject();
					objectIsOpen = true;
				}
			}
			else
			{
				if (ebene == aktKeyArray.length -1)
					g.writeStringField(name,props.get(Arrays.stream(aktKeyArray).collect(Collectors.joining("."))));
				else
				{
					g.writeFieldName(aktKeyArray[ebene]);
					g.writeStartObject();
					objectIsOpen = true;
				}
			}
			
			String [] nextKeyArray;
			if (ebene != aktKeyArray.length -1)
				nextKeyArray = handleEbene(aktKeyArray, ebene+1, props, keyIterator, g, arrayIsOpen);
			else
				nextKeyArray = getNextKey(keyIterator);

			if (equalKey(aktKeyArray, nextKeyArray, ebene))
			{
				arrayIsSameIndex = arrayIsOpen && aktKeyArray[ebene].compareTo(nextKeyArray[ebene]) == 0;
				if (objectIsOpen && !arrayIsSameIndex)
				{
					g.writeEndObject();
					objectIsOpen = false;
				}
				aktKeyArray = nextKeyArray;
			}
			else
			{
				if (objectIsOpen)
				{
					g.writeEndObject();
					objectIsOpen = false;
				}
				if (arrayIsOpen)
				{
					g.writeEndArray();
					arrayIsOpen = false;
				}
				return nextKeyArray;
			}
		}
		return getNextKey(keyIterator);  
	}	
	
	private static String ohneKlammern(String in)
	{
		return in.replaceAll("\\[.*\\]", "");
	}

	private static boolean isArray(String val)
	{
		return val != null && val.indexOf("[") >0;
	}


	private static boolean equalKey(String[] aktKeyArray,
			String[] nextKeyArray,
			int ebene)
	{
		boolean equal = true;
		for (int i = 0; i<= ebene && equal; i++)
			equal = equal
					&& i < aktKeyArray.length  
					&& i < nextKeyArray.length  
					&& (isArray(aktKeyArray[i]) && isArray(nextKeyArray[i]) && ohneKlammern(aktKeyArray[i]).compareTo(ohneKlammern(nextKeyArray[i])) == 0 ||  
					    aktKeyArray[i].compareTo(nextKeyArray[i]) == 0);
		return equal;
	}

	private static String[] getNextKey (Iterator<String> keyIterator)
	{
		if (keyIterator.hasNext())
		{
			String aktKey = keyIterator.next();
			if (aktKey != null)
			{
				return aktKey.split("\\.");
			}
		}
		return new String[]{};
	}
}
