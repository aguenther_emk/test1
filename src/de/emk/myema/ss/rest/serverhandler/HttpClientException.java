package de.emk.myema.ss.rest.serverhandler;

public class HttpClientException extends RuntimeException
{
	private static final long serialVersionUID = 1L;
	
	public HttpClientException(String message)
	{
		super(message);
	}
	public HttpClientException(String message, Throwable e )
	{
		super(message, e);
	}
}
