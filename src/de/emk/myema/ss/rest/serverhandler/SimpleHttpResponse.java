package de.emk.myema.ss.rest.serverhandler;

import java.util.HashMap;

public class SimpleHttpResponse
{
	private Integer status;
	private String content;
	private HashMap<String, String> headers;
	
	public SimpleHttpResponse(Integer _status, 
			String _content,
			HashMap<String, String> _headers)
	{
		super();
		this.status = _status;
		this.content = _content;
		this.headers = _headers;
	}

	public Integer getStatus()
	{
		return status;
	}

	public String getContent()
	{
		return content;
	}

	public HashMap<String, String> getHeaders()
	{
		return headers;
	}
}
