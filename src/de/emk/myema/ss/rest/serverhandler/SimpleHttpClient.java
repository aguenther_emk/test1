package de.emk.myema.ss.rest.serverhandler;

import java.net.HttpCookie;
import java.util.HashMap;


import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.ContentResponse;
import org.eclipse.jetty.client.api.Request;
import org.eclipse.jetty.client.util.StringContentProvider;
import org.eclipse.jetty.http.HttpFields;
import org.eclipse.jetty.http.HttpMethod;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleHttpClient
{
	Logger log = LoggerFactory.getLogger(this.getClass().getName()); 
	
	private static volatile HttpClient sslHttpClient;

	private synchronized static void initHttpClient() throws Exception
	{
		if (sslHttpClient == null)
		{
			// Instantiate and configure the SslContextFactory
			SslContextFactory sslContextFactory = new SslContextFactory();
			// Instantiate HttpClient with the SslContextFactory
			sslHttpClient = new HttpClient(sslContextFactory);
			
			// Configure HttpClient, for example:
			sslHttpClient.setFollowRedirects(false);
	
			// Start HttpClient
			sslHttpClient.start();
		}
	}
	
	public SimpleHttpResponse execRequest(
			String method, 
			String url, 
			String payload, 
			HashMap<String, String> headers,
			HashMap<String, String> cookies) throws Exception
	{
		log.info("request - method {}, urlExt {}, payload {}", method, url, payload);
		
		if (sslHttpClient == null)
			initHttpClient();
		
		Request request = sslHttpClient.newRequest(url)
		        .method(HttpMethod.fromString(method));
		
		if (payload != null)
			request.content(new StringContentProvider(payload));
		
		if (headers != null)
			headers.forEach((key, value) -> {request.getHeaders().add(key, value);});

		if (cookies != null)
			cookies.forEach((key, value) -> {request.cookie(new HttpCookie(key, value));});
		
		log(request);
		ContentResponse response = request.send();
		log(response);
		
		return new SimpleHttpResponse(response.getStatus(),
								      response.getContentAsString(),
								      convertHeaders2Map(response.getHeaders()));
	}

	private HashMap<String, String> convertHeaders2Map(HttpFields headers)
	{
		if (headers == null)
			return null;
		
		HashMap<String, String> ret = new HashMap<String, String>(); 
		headers.spliterator().forEachRemaining(x -> {ret.put(x.getName(), x.getValue());});
		return ret;
	}

	private static final int TRUCATE_SIZE = 500;
	private void log(ContentResponse response)
	{
		log.info("response - status {}, reason {}, version {}", response.getStatus(), response.getReason(), response.getVersion());
		if (response.getContent() != null)
		{
			if (response.getContent().length > TRUCATE_SIZE)
				log.info("response - content {} ... ", (response.getContentAsString().substring(0,TRUCATE_SIZE).replaceAll("\n", "").replaceAll("\r", "")));
			else
				log.info("response - content {}", response.getContentAsString().replaceAll("\n", "").replaceAll("\r", ""));
		}
		if (response.getHeaders() != null)
			response.getHeaders().forEach(x -> {log.info("response - header {}",x);});
	}

	private void log(Request request)
	{
		log.info("request - uri {}, server {}, method {}, path{} ", request.getURI(), request.getHost(), request.getMethod(), request.getPath());
		if (request.getParams() != null)
			request.getParams().forEach(x ->{log.info("request - params {}",x);});
		if (request.getContent() != null)
			request.getContent().forEach(x ->{log.info("request - body {}",x);});
		if (request.getHeaders() != null)
			request.getHeaders().forEach(x -> {log.info("request - header {}",x);});
		if (request.getCookies() != null)
			request.getCookies().forEach(x -> {log.info("request - cookies {}",x);});
	}
}
