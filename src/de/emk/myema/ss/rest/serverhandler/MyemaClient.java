package de.emk.myema.ss.rest.serverhandler;

import java.util.HashMap;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyemaClient
{
	Logger log = LoggerFactory.getLogger(this.getClass().getName()); 
	
	private String url;
	private String user;
	private String password;
	private String sessionCookie;
	private Supplier<SimpleHttpClient> clientSupplier;
	
	public MyemaClient(Supplier<SimpleHttpClient> _clientSupplier, String _url, String _user, String _password)
	{
		this.url = _url;
		this.user = _user;
		this.password = _password;
		this.clientSupplier =_clientSupplier;
	}
	
	public boolean isConnected()
	{
		if (sessionCookie != null)
			return true;
		else
			return false;
	}
	
	public boolean login()
	{
		SimpleHttpResponse ergebnis = request("POST", "/$directory/login", "[\"" + user + "\",\""+password + "\"]");
		
		if (ergebnis != null && 
		    ergebnis.getContent() != null && 
		    ergebnis.getContent().compareTo("{\"result\":true}") == 0)
			sessionCookie = ergebnis.getHeaders().get("WASID");
		return isConnected();
	}
	
	public void logout()
	{
		request("POST", "/$directory/logout", null);
		sessionCookie = null;
	}
	public SimpleHttpResponse request(
			String method, 
			String urlExt, 
			String payload)
	{
		try
		{
			HashMap<String, String> cookies = new HashMap<String, String>(); 
			if (sessionCookie != null)
				cookies.put("WASID", sessionCookie);
				
			SimpleHttpResponse resp = clientSupplier.get().execRequest( method, 
					 url + (urlExt == null ? "" :urlExt) , 
					 payload, 
					 null,
					 cookies); 
			
			checkResponse(resp);
			return resp; 
		}
		catch(Exception e)
		{
			throw new HttpClientException("Fehler bei Kommunikation mitt dem Server", e);
		}
	}

	private void checkResponse(SimpleHttpResponse resp)
	{
		if (resp == null || resp.getStatus() == null)
			throw new HttpClientException("kein Response oder Status nicht gesetzt");
		if (resp.getStatus() != 200)
			throw new HttpClientException("Fehler vom Server empfangen - Status != 200");
	}
}
